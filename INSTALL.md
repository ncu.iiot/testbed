# IIOT Testbed Backend 2.1

## Install Testbed Backend

### Install Steps
- Check Runtime Dependencies
  - DDS
  - Ubuntu Packages
  - Python2 Packages
- Configure Installation Setting
- Configure SSH Key-Based Authentication
- Install Testbed Backend to Runtime Directory
- Install Testbed Backend as Service

### Check Runtime Dependencies

### OS
OS | VERSION | BIT
:-|:-:|:-:
Ubuntu | 16.04 | 32

#### DDS
Vortex OpenSplice 6.9.0

#### Ubuntu Packages
Package | Version
-|-
make | 4.1-6
python | 2.7.12-1~16.04
python-pip | 8.1.1-2ubuntu0.4
openssh-client | 1:7.2p2-4ubuntu2.6

To install those  packages, do:
```bash
sudo apt install make python python-pip openssh-client
```

#### Python2 Packages
Package | Version
-|-
redis | 3.1.0

To install those packages, do:
``` bash
sudo -H pip install -r agentworker/requirements.txt
```

### Configure Installation Setting

Please modify these options in **Makefile** before you install:
- FRONTENDuser : Testbed Frontend Linux user.
- FRONTENDip : Testbed Frontend IP address.
- FRONTEND_REPORTdir : Testbed Frontend report directory
- REDISpasswd : Redis password.

Make sure these options are correct, Testbed won't work if any of them is wrong.

Example:
```Makefile
FRONTENDuser=testbed
FRONTENDip=127.0.0.1
FRONTEND_REPORTdir=$(TOPdir)/report
REDISpasswd=redis_password
```

### Configure SSH Key-Based Authentication
Testbed Backend send emulation result to frontend via scp command. To let this mechanism run automatically, Testbed Backend use key-based ssh authentication. Following commands will show you how to configure it:

```bash
# generate authentication key
ssh-keygen
# Copy public key to Testbed frontend
ssh-copy-id <FRONTENDuser>@<FRONTENDip>
```

### Install Testbed Backend to Runtime Directory
```bash
# setup OpenSplice runtime environment, please replace <OpenSplice-release.com> to correct path
source <Vortex_OpenSplice_release.com>
make install
```

### Install Testbed Backend As Service
Register Testbed backend as a systemd service and allow it to automatically run in boot.

```bash
sudo chmod 644 agentworker/testbed-agent.service
sudo cp -p agentworker/testbed-agent.service /lib/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable testbed-agent
sudo systemctl start testbed-agent
```

## Uninstall
Remove Testbed backend from **TOPdir**

```bash
make clean
```
