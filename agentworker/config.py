import getpass


class config:
    SERVER_USER_NAME = '{SERVER_USER_NAME}'
    SERVER_IP = '{SERVER_IP}'
    REDIS_PORT = 6379
    REDIS_PASSWORD = '{REDIS_PASSWORD}'
    TESTBED_PATH = '{TOPdir}/testbed'
    REPORT_PATH = TESTBED_PATH + '/report/'
    FRONTEND_REPORT_PATH = '{FRONTEND_REPORTdir}'
    DDS_PATH = TESTBED_PATH + '/emulation'
    DDS_READY_PATH = TESTBED_PATH + '/ready'
    DDS_START_PATH = TESTBED_PATH + '/start'
    DATASETTING_PATH = TESTBED_PATH + '/datasetting/datasetting'
