#include "DDSEntityManager.h"
#include <iostream>

using namespace std;

DDSEntityManager::DDSEntityManager()
{
  createParticipant("Emulation example");
  getDefaultTopicQos();
}

void DDSEntityManager::createParticipant(const char *partitiontName)
{
  domain = DOMAIN_ID_DEFAULT;
  dpf = DomainParticipantFactory::get_instance();
  checkHandle(dpf.in(), "DDS::DomainParticipantFactory::get_instance");
  participant = dpf->create_participant(domain, PARTICIPANT_QOS_DEFAULT, NULL,
                                        STATUS_MASK_NONE);
  checkHandle(participant.in(),
              "DDS::DomainParticipantFactory::create_participant");
  partition = partitiontName;
}

void DDSEntityManager::deleteParticipant()
{
  status = dpf->delete_participant(participant.in());
  checkStatus(status, "DDS::DomainParticipant::delete_participant ");
}

void DDSEntityManager::registerType(TypeSupport *ts)
{
  typeName = ts->get_type_name();
  status = ts->register_type(participant.in(), typeName);
  checkStatus(status, "register_type");
}

void DDSEntityManager::createTopic(char *topicName)
{
  topic = participant->create_topic(topicName, typeName, setting_topic_qos,
                                    NULL, STATUS_MASK_NONE);
  checkHandle(topic.in(), "DDS::DomainParticipant::create_topic ()");
}

void DDSEntityManager::deleteTopic()
{
  status = participant->delete_topic(topic);
  checkStatus(status, "DDS.DomainParticipant.delete_topic");
}

void DDSEntityManager::createPublisher()
{
  status = participant->get_default_publisher_qos(pub_qos);
  checkStatus(status, "DDS::DomainParticipant::get_default_publisher_qos");
  pub_qos.partition.name.length(1);
  pub_qos.partition.name[0] = partition;
  publisher = participant->create_publisher(pub_qos, NULL, STATUS_MASK_NONE);
  checkHandle(publisher.in(), "DDS::DomainParticipant::create_publisher");
}

void DDSEntityManager::deletePublisher()
{
  status = participant->delete_publisher(publisher.in());
  checkStatus(status, "DDS::DomainParticipant::delete_publisher ");
}

void DDSEntityManager::createWriter()
{
  writer = publisher->create_datawriter(topic.in(),
                                        DATAWRITER_QOS_USE_TOPIC_QOS, NULL, STATUS_MASK_NONE);
  checkHandle(writer, "DDS::Publisher::create_datawriter");
}

void DDSEntityManager::createWriter(bool autodispose_unregistered_instances)
{
  status = publisher->get_default_datawriter_qos(dw_qos);
  checkStatus(status, "DDS::DomainParticipant::get_default_publisher_qos");
  status = publisher->copy_from_topic_qos(dw_qos, setting_topic_qos);
  checkStatus(status, "DDS::Publisher::copy_from_topic_qos");
  // Set autodispose to false so that you can start
  // the subscriber after the publisher
  dw_qos.writer_data_lifecycle.autodispose_unregistered_instances =
      autodispose_unregistered_instances;
  writer = publisher->create_datawriter(topic.in(), dw_qos, NULL,
                                        STATUS_MASK_NONE);
  checkHandle(writer, "DDS::Publisher::create_datawriter");
}

void DDSEntityManager::deleteWriter()
{
  status = publisher->delete_datawriter(writer);
  checkStatus(status, "DDS::Publisher::delete_datawriter ");
}

void DDSEntityManager::createSubscriber()
{
  int status = participant->get_default_subscriber_qos(sub_qos);
  checkStatus(status, "DDS::DomainParticipant::get_default_subscriber_qos");
  sub_qos.partition.name.length(1);
  sub_qos.partition.name[0] = partition;
  subscriber = participant->create_subscriber(sub_qos, NULL, STATUS_MASK_NONE);
  checkHandle(subscriber.in(), "DDS::DomainParticipant::create_subscriber");
}

void DDSEntityManager::deleteSubscriber()
{
  status = participant->delete_subscriber(subscriber);
  checkStatus(status, "DDS::DomainParticipant::delete_subscriber ");
}

void DDSEntityManager::createReader()
{
  status = subscriber->get_default_datareader_qos(dr_qos);
  checkStatus(status, "DDS::DomainParticipant::get_defaultreader_qos");
  status = subscriber->copy_from_topic_qos(dr_qos, setting_topic_qos);
  checkStatus(status, "DDS::DomainParticipant::compy_from_topic_qos");
  reader = subscriber->create_datareader(topic.in(),
                                         DATAREADER_QOS_USE_TOPIC_QOS, NULL, STATUS_MASK_NONE);
  checkHandle(reader, "DDS::Subscriber::create_datareader ()");
}

void DDSEntityManager::deleteReader()
{
  status = subscriber->delete_datareader(reader);
  checkStatus(status, "DDS::Subscriber::delete_datareader ");
}

void DDSEntityManager::getDefaultTopicQos()
{
  status = participant->get_default_topic_qos(setting_topic_qos);
  checkStatus(status, "DDS::DomainParticipant::get_default_topic_qos");
}

void DDSEntityManager::setDefaultTopicQos()
{
  cout << "set default topic qos: ===========" << endl;
  printQos(setting_topic_qos);
  status = participant->set_default_topic_qos(setting_topic_qos);
  checkStatus(status, "DDS::DomainParticipant::set_default_topic_qos");
}

void DDSEntityManager::setQoSReliability(int kind)
{
  switch (kind)
  {
  case 1:
    setting_topic_qos.reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    break;
  case 2:
    setting_topic_qos.reliability.kind = RELIABLE_RELIABILITY_QOS;
    break;
  default:
    setting_topic_qos.reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    break;
  }
}

void DDSEntityManager::setQoSOwnership(int kind)
{
  switch (kind)
  {
  case 1:
    setting_topic_qos.ownership.kind = SHARED_OWNERSHIP_QOS;
    break;
  case 2:
    setting_topic_qos.ownership.kind = EXCLUSIVE_OWNERSHIP_QOS;
    break;
  default:
    setting_topic_qos.ownership.kind = SHARED_OWNERSHIP_QOS;
    break;
  }
}

void DDSEntityManager::setQoSDurability(int kind)
{
  DurabilityQosPolicyKind temp;
  switch (kind)
  {
  case 1:
    temp = VOLATILE_DURABILITY_QOS;
    break;
  case 2:
    temp = TRANSIENT_LOCAL_DURABILITY_QOS;
    break;
  case 3:
    temp = TRANSIENT_DURABILITY_QOS;
    break;
  case 4:
    temp = PERSISTENT_DURABILITY_QOS;
    break;
  default:
    temp = VOLATILE_DURABILITY_QOS;
    break;
  }
  setting_topic_qos.durability.kind = temp;
}

void DDSEntityManager::setQoSLiveliness(int kind, long sec, unsigned long nsec)
{
  switch (kind)
  {
  case 1:
    setting_topic_qos.liveliness.kind = AUTOMATIC_LIVELINESS_QOS;
    break;
  case 2:
    setting_topic_qos.liveliness.kind = MANUAL_BY_PARTICIPANT_LIVELINESS_QOS;
    break;
  case 3:
    setting_topic_qos.liveliness.kind = MANUAL_BY_TOPIC_LIVELINESS_QOS;
    break;
  default:
    setting_topic_qos.liveliness.kind = AUTOMATIC_LIVELINESS_QOS;
    break;
  }

  Duration_t temp;
  temp.sec = sec;
  temp.nanosec = nsec;
  setting_topic_qos.liveliness.lease_duration = temp;
}

void DDSEntityManager::setQoSDeadline(long sec, unsigned long nsec)
{
  Duration_t temp;
  temp.sec = sec;
  temp.nanosec = nsec;
  setting_topic_qos.deadline.period = temp;
}

void DDSEntityManager::setQoSHistory(int kind, long depth)
{
  switch (kind)
  {
  case 1:
    setting_topic_qos.history.kind = KEEP_LAST_HISTORY_QOS;
    break;
  case 2:
    setting_topic_qos.history.kind = KEEP_ALL_HISTORY_QOS;
    break;
  default:
    setting_topic_qos.history.kind = KEEP_LAST_HISTORY_QOS;
    break;
  }

  setting_topic_qos.history.depth = depth;
}

void DDSEntityManager::setQoSResourceLimit(long maxSample, long maxInstance, long maxInstancePerSample)
{
  setting_topic_qos.resource_limits.max_samples = maxSample;
  setting_topic_qos.resource_limits.max_instances = maxInstance;
  setting_topic_qos.resource_limits.max_samples_per_instance = maxInstancePerSample;
}

void DDSEntityManager::setQoSLatencyBudget(long sec, unsigned long nsec)
{
  Duration_t temp;
  temp.sec = (sec == 2147483647 ? 0 : sec);
  temp.nanosec = (nsec == 2147483647 ? 0 : nsec);
  setting_topic_qos.latency_budget.duration = temp;
}

void DDSEntityManager::setQoSLifespan(long sec, unsigned long nsec)
{
  Duration_t temp;
  temp.sec = sec;
  temp.nanosec = nsec;
  setting_topic_qos.lifespan.duration = temp;
}

void DDSEntityManager::setQoSDestinationOrder(int kind)
{
  switch (kind)
  {
  case 1:
    setting_topic_qos.destination_order.kind = BY_RECEPTION_TIMESTAMP_DESTINATIONORDER_QOS;
    break;
  case 2:
    setting_topic_qos.destination_order.kind = BY_SOURCE_TIMESTAMP_DESTINATIONORDER_QOS;
    break;
  default:
    setting_topic_qos.destination_order.kind = BY_RECEPTION_TIMESTAMP_DESTINATIONORDER_QOS;
    break;
  }
}

void DDSEntityManager::setQoSTransportPriority(long value)
{
  setting_topic_qos.transport_priority.value = value;
}

void DDSEntityManager::setQoSDurabilityService(long sec, unsigned long nsec, int historyKind, long historyDepth, long maxSample, long maxInstance, long maxSamplesPerInstance)
{
  Duration_t serviceCleanupDelaySec;
  serviceCleanupDelaySec.sec = sec;
  serviceCleanupDelaySec.nanosec = nsec;
  setting_topic_qos.durability_service.service_cleanup_delay = serviceCleanupDelaySec;
  switch (historyKind)
  {
  case 1:
    setting_topic_qos.durability_service.history_kind = KEEP_LAST_HISTORY_QOS;
    break;
  case 2:
    setting_topic_qos.durability_service.history_kind = KEEP_ALL_HISTORY_QOS;
    break;
  default:
    setting_topic_qos.durability_service.history_kind = KEEP_LAST_HISTORY_QOS;
    break;
  }

  setting_topic_qos.durability_service.history_depth = (historyDepth <= 0 ? 1 : historyDepth);
  setting_topic_qos.durability_service.max_samples = (maxSample <= 0 ? -1 : maxSample);
  setting_topic_qos.durability_service.max_instances = (maxInstance <= 0 ? -1 : maxInstance);
  setting_topic_qos.durability_service.max_samples_per_instance = (maxSamplesPerInstance <= 0 ? -1 : maxSamplesPerInstance);
}

void DDSEntityManager::setQoSTopicData(const string value)
{
  for (unsigned int i = 0; i < value.size(); ++i)
  {
    DDS::OpenSplice::Utils::appendSequenceItem(setting_topic_qos.topic_data.value, value[i]);
  }
}

bool DDSEntityManager::checkDwQoSSetting()
{
  if (dw_qos.reliability.kind == setting_topic_qos.reliability.kind &&
      dw_qos.ownership.kind == setting_topic_qos.ownership.kind &&
      dw_qos.durability.kind == setting_topic_qos.durability.kind &&
      dw_qos.liveliness.kind == setting_topic_qos.liveliness.kind)
    return true;
  else
    return false;
}

DataReader_ptr DDSEntityManager::getReader()
{
  return DataReader::_duplicate(reader.in());
}

DataWriter_ptr DDSEntityManager::getWriter()
{
  return DataWriter::_duplicate(writer.in());
}

Publisher_ptr DDSEntityManager::getPublisher()
{
  return Publisher::_duplicate(publisher.in());
}

Subscriber_ptr DDSEntityManager::getSubscriber()
{
  return Subscriber::_duplicate(subscriber.in());
}

Topic_ptr DDSEntityManager::getTopic()
{
  return Topic::_duplicate(topic.in());
}

DomainParticipant_ptr DDSEntityManager::getParticipant()
{
  return DomainParticipant::_duplicate(participant.in());
}

DDSEntityManager::~DDSEntityManager()
{
}

void DDSEntityManager::printQos(TopicQos qos)
{
  cout << "reliability: " << (qos.reliability.kind == RELIABLE_RELIABILITY_QOS) << endl;
  cout << "ownership: " << (qos.ownership.kind == SHARED_OWNERSHIP_QOS) << endl;
  cout << "durability: " << (qos.durability.kind == TRANSIENT_LOCAL_DURABILITY_QOS) << endl;
  cout << "liveliness: " << (qos.liveliness.kind == AUTOMATIC_LIVELINESS_QOS) << endl;
  cout << "deadline sec: " << qos.deadline.period.sec << endl;
  cout << "deadline nsec: " << qos.deadline.period.nanosec << endl;
  cout << "history: " << (qos.history.kind == KEEP_LAST_HISTORY_QOS) << endl;
  cout << "history depth: " << qos.history.depth << endl;
  cout << "resource_limits max_samples : " << qos.resource_limits.max_samples << endl;
  cout << "resource_limits max_instances : " << qos.resource_limits.max_instances << endl;
  cout << "resource_limits max_samples_per_instance : " << qos.resource_limits.max_samples_per_instance << endl;
  cout << "latency_budget sec: " << qos.latency_budget.duration.sec << endl;
  cout << "latency_budget nsec: " << qos.latency_budget.duration.nanosec << endl;
  cout << "lifespan sec: " << qos.lifespan.duration.sec << endl;
  cout << "lifespan nsec: " << qos.lifespan.duration.nanosec << endl;
  cout << "destination_order: " << (qos.destination_order.kind == BY_RECEPTION_TIMESTAMP_DESTINATIONORDER_QOS) << endl;
  cout << "transport_priority: " << qos.transport_priority.value << endl;
  cout << "durability_service service_cleanup_delay sec: " << qos.durability_service.service_cleanup_delay.sec << endl;
  cout << "durability_service service_cleanup_delay nsec: " << qos.durability_service.service_cleanup_delay.nanosec << endl;
  cout << "durability_service history_kind: " << (qos.durability_service.history_kind == KEEP_LAST_HISTORY_QOS) << endl;
  cout << "durability_service history_depth : " << qos.durability_service.history_depth << endl;
  cout << "durability_service max_samples : " << qos.durability_service.max_samples << endl;
  cout << "durability_service max_instances : " << qos.durability_service.max_instances << endl;
  cout << "durability_service max_samples_per_instance : " << qos.durability_service.max_samples_per_instance << endl;
}
