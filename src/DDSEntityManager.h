
#ifndef _DDSENTITYMGR_
#define _DDSENTITYMGR_

#include "ccpp_dds_dcps.h"
#include "SequenceUtils.h"
#include "CheckStatus.h"
using namespace DDS;

class DDSEntityManager
{

  /* Generic DDS entities */
  DomainParticipantFactory_var dpf;
  DomainParticipant_var participant;
  Topic_var topic;
  Publisher_var publisher;
  Subscriber_var subscriber;
  DataWriter_var writer;
  DataReader_var reader;

  /* QosPolicy holders */
  TopicQos reliable_topic_qos;
  TopicQos setting_topic_qos;
  PublisherQos pub_qos;
  DataWriterQos dw_qos;
  SubscriberQos sub_qos;
  DataReaderQos dr_qos;

  DomainId_t domain;
  ReturnCode_t status;

  DDS::String_var partition;
  DDS::String_var typeName;

public:
  DDSEntityManager();
  void createParticipant(const char *partitiontName);
  void deleteParticipant();
  void registerType(TypeSupport *ts);
  void createTopic(char *topicName);
  void deleteTopic();
  void createPublisher();
  void deletePublisher();
  void createWriter();
  void createWriter(bool autodispose_unregistered_instances);
  void deleteWriter();
  void createSubscriber();
  void deleteSubscriber();
  void createReader();
  void deleteReader();
  void getDefaultTopicQos();
  void setDefaultTopicQos();
  void setQoSReliability(int kind);
  void setQoSOwnership(int kind);
  void setQoSDurability(int kind);
  void setQoSLiveliness(int kind, long sec, unsigned long nsec);
  void setQoSDeadline(long sec, unsigned long nsec);
  void setQoSHistory(int kind, long depth);
  void setQoSResourceLimit(long maxSample, long maxInstance, long maxInstancePerSample);
  void setQoSLatencyBudget(long sec, unsigned long nsec);
  void setQoSLifespan(long sec, unsigned long nsec);
  void setQoSDestinationOrder(int kind);
  void setQoSTransportPriority(long value);
  void setQoSDurabilityService(long sec, unsigned long nsec, int historyKind, long historyDepth, long maxSample, long maxInstance, long maxSamplesPerInstance);
  void setQoSTopicData(const string value);
  bool checkDwQoSSetting();
  void printQos(TopicQos qos);
  DataReader_ptr getReader();
  DataWriter_ptr getWriter();
  Publisher_ptr getPublisher();
  Subscriber_ptr getSubscriber();
  Topic_ptr getTopic();
  DomainParticipant_ptr getParticipant();
  ~DDSEntityManager();
};

#endif
