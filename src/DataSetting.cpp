#include "DataSetting.h"

DataSetting::DataSetting()
{
}

DataSetting::~DataSetting()
{
}

void DataSetting::setDeviceName(string name)
{
  this->deviceName = name;
}

string DataSetting::getDeviceName()
{
  return this->deviceName;
}

void DataSetting::setDeviceID(int id)
{
  this->deviceID = id;
}

int DataSetting::getDeviceID()
{
  return this->deviceID;
}

void DataSetting::setExperimentTime(int time)
{
  this->experimentTime = time;
}

int DataSetting::getExperimentTime()
{
  return this->experimentTime;
}

void DataSetting::setQosSettingNum(int num)
{
  this->qosSettingNum = num;
}

int DataSetting::getQosSettingNum()
{
  return this->qosSettingNum;
}

void DataSetting::setPubNum(int num)
{
  if (num >= 0)
  {
    this->pubNum = num;
    this->pubdata = new PubData[this->pubNum];

    for (int i = 0; i < this->pubNum; i++)
      this->pubdata[i].cycleTime = 0;
  }
  else
    throw;
}

int DataSetting::getPubNum()
{
  return this->pubNum;
}

void DataSetting::setPubTopicNameAt(int index, string name)
{
  if (0 <= index && index < this->pubNum)
    this->pubdata[index].topicName = name;
  else
    throw;
}

string DataSetting::getPubTopicNameAt(int index)
{
  if (0 <= index && index < this->pubNum)
    return this->pubdata[index].topicName;
  else
    throw;
}

void DataSetting::setPubMessageAt(int index, int messageSize)
{
  string message = "";
  for (int i = 0; i < messageSize; i++)
    message = message + "@";

  if (0 <= index && index < this->pubNum)
    this->pubdata[index].message = message;
  else
    throw;
}

string DataSetting::getPubMessageAt(int index)
{
  return this->pubdata[index].message;
}

void DataSetting::setPubFrequencyAt(int index, int cycleTime)
{
  if (0 <= index && index < this->pubNum)
    this->pubdata[index].cycleTime = cycleTime;
  else
    throw;
}

int DataSetting::getPubFrequencyAt(int index)
{
  return this->pubdata[index].cycleTime;
}

void DataSetting::setSubNum(int num)
{
  if (num >= 0)
  {
    this->subNum = num;
    this->subdata = new SubData[this->subNum];
  }
  else
    throw;
}

int DataSetting::getSubNum()
{
  return this->subNum;
}

void DataSetting::setSubTopicNameAt(int index, string name)
{
  if (0 <= index && index < subNum)
    this->subdata[index].topicName = name;
  else
    throw;
}

string DataSetting::getSubTopicNameAt(int index)
{
  if (0 <= index && index < this->subNum)
    return this->subdata[index].topicName;
  else
    throw;
}

void DataSetting::setSubSaveFlagAt(int index, bool flag)
{
  this->subdata[index].saved = flag;
}
