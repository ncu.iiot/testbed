#ifndef DATASETTING_H
#define DATASETTING_H

#include <string>
using namespace std;

typedef struct _publisher_data_
{
  int cycleTime;
  int timeCount;
  string topicName;
  string message;
} PubData;

typedef struct _subscriber_data_
{
  string topicName;
  bool saved;
} SubData;

class DataSetting
{
public:
  DataSetting();
  ~DataSetting();

  void setDeviceName(string name);
  string getDeviceName();

  void setDeviceID(int id);
  int getDeviceID();

  void setExperimentTime(int time);
  int getExperimentTime();

  void setQosSettingNum(int num);
  int getQosSettingNum();

  void setPubNum(int num);
  int getPubNum();

  void setPubTopicNameAt(int index, string name);
  string getPubTopicNameAt(int index);

  void setPubMessageAt(int index, int messageSize);
  string getPubMessageAt(int index);

  void setPubFrequencyAt(int index, int cycleTime);
  int getPubFrequencyAt(int index);

  void setSubNum(int num);
  int getSubNum();

  void setSubTopicNameAt(int index, string name);
  string getSubTopicNameAt(int index);
  void setSubSaveFlagAt(int index, bool flag);

private:
  string deviceName;
  int deviceID;
  int experimentTime;
  int pubNum;
  int subNum;
  int qosSettingNum;
  PubData *pubdata;
  SubData *subdata;
};
#endif
