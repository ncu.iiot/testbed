#include "Device.h"

Device::Device()
{
  cpuInfo.clear();
  memInfo.clear();
  inBandwidthInfo.clear();
  outBandwidthInfo.clear();
}

Device::~Device()
{
}

void Device::setCPU(double cpu)
{
  cpuInfo.push_back(cpu);
}

double Device::getCPUAt(int index)
{
  return cpuInfo[index];
}

void Device::setMem(double mem)
{
  memInfo.push_back(mem);
}

double Device::getMemAt(int index)
{
  return memInfo[index];
}

void Device::setInBandwidth(int bandwidth)
{
  inBandwidthInfo.push_back(bandwidth);
}

int Device::getInBandwidthInfoAt(int index)
{
  return inBandwidthInfo[index];
}

void Device::setOutBandwidth(int bandwidth)
{
  outBandwidthInfo.push_back(bandwidth);
}

int Device::getOutBandwidthInfoAt(int index)
{
  return outBandwidthInfo[index];
}

int Device::getDeviceSize()
{
  return cpuInfo.size();
}
