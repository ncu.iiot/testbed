/************************************************************************
 * LOGICAL_NAME:    Emulation.cpp
 * FUNCTION:        Run the emulation by user's arguments.
 * Version:         Testbed 2.1
 * DATE             2019 01 16
 ************************************************************************
 *
 * This file contains the implementation for the 'Emulation' executable.
 *
 ***/
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <thread>
#include <vector>
#include <queue>
#include <unistd.h>
#include <stdexcept>
#include <functional>
#include <stdlib.h>
#include <mutex>
#include <jsoncpp/json/json.h>

#include "DDSEntityManager.h"
#include "ccpp_EmulationData.h"
#include "QosSetting.h"
#include "DataSetting.h"
#include "Topics.h"
#include "Device.h"
#include "Timer.h"
#include "Probe.h"
#include "Handler.h"
#include "vortex_os.h"

#include "example_main.h"
#include "sys/types.h"
#include "sys/sysinfo.h"
#include "sys/timerfd.h"

using namespace DDS;
using namespace EmulationData;

mutex mtxProbingQueue;

#define PUBLISH 1
#define SUBSCRIBE 2
#define MEASURE_CONDITION 100
#define BYTE 8
#define DATA_SETTING_DIR "{TOPdir}/testbed/datasetting/datasetting"
#define REPORT_DIR "{TOPdir}/testbed/report/"
#define READY_PATH "{TOPdir}/testbed/ready"
#define START_PATH "{TOPdir}/testbed/start"
#define NETWORK_INTERFACE "{ETH0}"
// #define DISPLAY_INFO 1

/* entry point exported and demangled so symbol can be found in shared library */
extern "C"
{
  OS_API_EXPORT
  int EmulationDataPublisher(int argc, char *argv[]);
}

struct ProbContent
{
  string topicName;
  int destDeviceID;
  long probID;
  long probLatencyIndex;
};

void loadHeader(DataSetting *dataSetting, string line)
{
  stringstream ss(line);
  string token;

  ss >> token;
  dataSetting->setDeviceName(token);

  ss >> token;
  dataSetting->setDeviceID(atoi(token.c_str()));

  ss >> token;
  dataSetting->setPubNum(atoi(token.c_str()));

  ss >> token;
  dataSetting->setSubNum(atoi(token.c_str()));

  ss >> token;
  dataSetting->setExperimentTime(atoi(token.c_str()));

  ss >> token;
  dataSetting->setQosSettingNum(atoi(token.c_str()));
}

void loadPub(DataSetting *dataSetting, string line, int i)
{
  stringstream ss(line);
  string token;

  ss >> token;
  dataSetting->setPubTopicNameAt(i, token);

  ss >> token;
  dataSetting->setPubMessageAt(i, atoi(token.c_str()));

  ss >> token;
  dataSetting->setPubFrequencyAt(i, atoi(token.c_str()));
}

void loadSub(DataSetting *dataSetting, string line, int i)
{
  stringstream ss(line);
  string token;

  ss >> token;
  dataSetting->setSubTopicNameAt(i, token);

  ss >> token;
  if (token.compare("true") == 0 || token.compare("True") == 0)
    dataSetting->setSubSaveFlagAt(i, true);
  else
    dataSetting->setSubSaveFlagAt(i, false);
}

bool loadFile(DataSetting *dataSetting, QosSetting *&qosSetting)
{
  bool result = false;
  string path = DATA_SETTING_DIR;
  ifstream file(path);
  string line;
  int qosSettingNum = 0;

  if (!file)
  {
    cout << "File open error" << endl;
  }
  else
  {
    getline(file, line);
    loadHeader(dataSetting, line);
    qosSettingNum = dataSetting->getQosSettingNum();

    for (int i = 0; i < dataSetting->getPubNum(); i++)
    {
      getline(file, line);
      loadPub(dataSetting, line, i);
    }

    for (int i = 0; i < dataSetting->getSubNum(); i++)
    {
      getline(file, line);
      loadSub(dataSetting, line, i);
    }

    if (qosSettingNum > 0)
    {
      qosSetting = new QosSetting[qosSettingNum+1];

      for (int i = 0; i < qosSettingNum; i++)
      {
        getline(file, line);
        qosSetting[i].setQosSetting(line);
      }
      qosSetting[qosSettingNum].setQosSetting(line);
    }


    file.close();
    result = true;
  }

  return result;
}

static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;

void initCpuValue()
{
  FILE *file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu", &lastTotalUser, &lastTotalUserLow,
         &lastTotalSys, &lastTotalIdle);
  fclose(file);
}

double getCpuValue()
{
  double percent;
  FILE *file;

  unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;

  file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow,
         &totalSys, &totalIdle);
  fclose(file);

  if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
      totalSys < lastTotalSys || totalIdle < lastTotalIdle)
  {
    //Overflow detection. Just skip this value.
    percent = -1.0;
  }
  else
  {
    total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) +
            (totalSys - lastTotalSys);
    percent = total;
    total += (totalIdle - lastTotalIdle);
    percent /= total;
    percent *= 100;
  }

  return percent;
}

double getMemoryValue()
{
  struct sysinfo memInfo;
  double percent;

  sysinfo(&memInfo);
  long long totalVirtualMem = memInfo.totalram;
  //Add other values in next statement to avoid int overflow on right hand side...
  totalVirtualMem += memInfo.totalswap;
  totalVirtualMem *= memInfo.mem_unit;

  long long virtualMemUsed = memInfo.totalram - memInfo.freeram;
  //Add other values in next statement to avoid int overflow on right hand side...
  virtualMemUsed += memInfo.totalswap - memInfo.freeswap;
  virtualMemUsed *= memInfo.mem_unit;

  percent = ((double)virtualMemUsed / (double)totalVirtualMem) * 100;
  return percent;
}

void setReady()
{
  ofstream file(READY_PATH);
  if (!file)
    cout << "Failed to set ready\n";
  else
  {
    file << "ready\n";
    file.close();
  }
}

void waitStart()
{
  os_time delay_1s = {1, 0};
  while (1)
  {
    ifstream file(START_PATH);
    if (file.good())
      break;
    os_nanoSleep(delay_1s);
  }
}

void setQoS(int type, DDSEntityManager *mgr, DataSetting *dataSetting, QosSetting *qosSetting)
{
  int topicNum = (type == PUBLISH ? dataSetting->getPubNum() : dataSetting->getSubNum());
  int qosConut = 0;

  for (int i = 0; i < topicNum + 1; i++)
  {
    mgr[i].setQoSReliability(qosSetting[qosConut].getReliableKind());
    mgr[i].setQoSOwnership(qosSetting[qosConut].getOwnershipKind());
    mgr[i].setQoSDurability(qosSetting[qosConut].getDurabilityKind());
    mgr[i].setQoSLiveliness(qosSetting[qosConut].getLivelinessKind(),
                            qosSetting[qosConut].getLivelinessSec(), qosSetting[qosConut].getLivelinessNsec());
    mgr[i].setQoSHistory(qosSetting[qosConut].getHistoryKind(), qosSetting[qosConut].getHistoryDepth());
    mgr[i].setQoSResourceLimit(qosSetting[qosConut].getResourceLimitSample(),
                               qosSetting[qosConut].getResourceLimitInstance(),
                               qosSetting[qosConut].getResourceLimitSamplePerInstance());
    mgr[i].setQoSLatencyBudget(qosSetting[qosConut].getLatencyBudgetSec(),
                               qosSetting[qosConut].getLatencyBudgetNsec());
    mgr[i].setQoSLifespan(qosSetting[qosConut].getLifespanSec(), qosSetting[qosConut].getLifespanNsec());
    mgr[i].setQoSDestinationOrder(qosSetting[qosConut].getDestinationOrderKind());
    mgr[i].setQoSTransportPriority(qosSetting[qosConut].getTransportPriorityValue());
    mgr[i].setQoSDurabilityService(qosSetting[qosConut].getDurabilityServiceSec(),
                                   qosSetting[qosConut].getDurabilityServiceNsec(),
                                   qosSetting[qosConut].getDurabilityServiceHistoryKind(),
                                   qosSetting[qosConut].getDurabilityServiceHistoryDepth(),
                                   qosSetting[qosConut].getDurabilityServiceMaxSamples(),
                                   qosSetting[qosConut].getDurabilityServiceMaxInstances(),
                                   qosSetting[qosConut].getDurabilityServiceMaxSamplesPerInstance());
    mgr[i].setQoSTopicData(qosSetting[qosConut].getTopicDataValue());
    mgr[i].setDefaultTopicQos();
    qosConut++;
  }
}

void publish(DDSEntityManager &mgr, Topics &topicRecord, int deviceID, string msg,
             queue<ProbContent> &probingQueue, Probe &probingMessage, int &measureCount)
{
  DataWriter_var dwriter = mgr.getWriter();

  char *topicName = (mgr.getTopic())->get_name();

  if (strcmp(topicName, "measure") != 0)
  {
    MsgDataWriter_var EmulationWriter = MsgDataWriter::_narrow(dwriter.in());

    Msg msgInstance;
    stringstream message;
    message << msg;
    msgInstance.message = DDS::string_dup((message.str()).c_str());
    msgInstance.deviceID = deviceID;
    msgInstance.msgID = topicRecord.getPubCount(string(topicName));

    if (measureCount == MEASURE_CONDITION)
    {
      msgInstance.probID = probingMessage.addTimestamp();
      measureCount = 1;
    }
    else
    {
      msgInstance.probID = 0;
      measureCount++;
    }

#ifdef DISPLAY_INFO
    cout << "=== [Publisher] writing a message containing :" << endl;
    cout << "    deviceID  : " << msgInstance.deviceID << endl;
    cout << "    Message : \"" << msgInstance.message << "\"" << endl;
#endif

    ReturnCode_t status = EmulationWriter->write(msgInstance, DDS::HANDLE_NIL);
    checkStatus(status, "MsgDataWriter::write");
    topicRecord.addPubCount(string(topicName));
  }
  else
  {
    ProbMsgDataWriter_var EmulationWriter = ProbMsgDataWriter::_narrow(dwriter.in());

    // Resend
    if (!probingQueue.empty())
    {
      // pub measure topic
      ProbMsg msgInstance;
      stringstream message;
      ProbContent temp;

      temp = probingQueue.front();

      unique_lock<std::mutex> m_lock(mtxProbingQueue);
      probingQueue.pop();
      m_lock.unlock();

      msgInstance.srcDeviceID = deviceID;
      msgInstance.destDeviceID = temp.destDeviceID;
      msgInstance.probID = temp.probID;
      msgInstance.latency = probingMessage.measure(temp.probLatencyIndex).tv_nsec;

      message << temp.topicName;
      msgInstance.topicName = DDS::string_dup((message.str()).c_str());

      ReturnCode_t status = EmulationWriter->write(msgInstance, DDS::HANDLE_NIL);
      checkStatus(status, "MsgDataWriter::write");
    }
  }
}

bool gExperimentEnd;

void subscribe(DDSEntityManager &mgr, Topics &topicRecord, int deviceID,
               queue<ProbContent> &probingQueue, Probe &probingMessage)
{
  SampleInfoSeq infoSeq;
  ReturnCode_t status = -1;
  bool hasSecondChance = false;

  ConditionSeq_var conditionList = new ConditionSeq;
  StatusCondition_var emulation_sc;
  WaitSet w;
  // 1.5 millisec
  Duration_t wait_timeout = {0, 002000000};
  os_time delay_1us = {0, 000100000};

  char *topicName = (mgr.getTopic())->get_name();

  while (!hasSecondChance)
  {
    if (gExperimentEnd)
    {
      hasSecondChance = true;
      os_nanoSleep(delay_1us);
    }

    DataReader_var dreader = mgr.getReader();

    if (strcmp(topicName, "measure") != 0)
    {
      MsgDataReader_var EmulationReader = MsgDataReader::_narrow(dreader.in());
      MsgSeq msgList;

      // Add datareader statuscondition to waitset
      emulation_sc = EmulationReader->get_statuscondition();
      emulation_sc->set_enabled_statuses(DATA_AVAILABLE_STATUS);
      status = w.attach_condition(emulation_sc);
      status = w.wait(conditionList.inout(), wait_timeout);

      status = EmulationReader->take(msgList, infoSeq, 1,
                                     NOT_READ_SAMPLE_STATE, ANY_VIEW_STATE, ANY_INSTANCE_STATE);
      checkStatus(status, "msgDataReader::take");

      if (status == DDS::RETCODE_ALREADY_DELETED)
      {
        continue;
      }

      if (msgList.length() > 0 && infoSeq[0].valid_data && status == DDS::RETCODE_OK)
      {
#ifdef DISPLAY_INFO
        cout << "=== [Subscriber] message received :" << endl;
        cout << "    Time  : " << topicRecord.getLatencySize(string(topicName)) << endl;
        cout << "    deviceID  : " << msgList[0].deviceID << endl;
        cout << "    Message : \"" << msgList[0].message << "\"" << endl;
#endif

        if (topicRecord.addSubMsgID(string(topicName), msgList[0].deviceID, msgList[0].msgID))
        {
          topicRecord.addSubCount(string(topicName), msgList[0].deviceID);
          if (msgList[0].probID != 0)
          {
            ProbContent temp;
            temp.destDeviceID = msgList[0].deviceID;
            temp.topicName = string(topicName);
            temp.probID = msgList[0].probID;
            temp.probLatencyIndex = probingMessage.addTimestamp();

            unique_lock<std::mutex> m_lock(mtxProbingQueue);
            probingQueue.push(temp);
            m_lock.unlock();
          }
        }
      }
      status = EmulationReader->return_loan(msgList, infoSeq);
    }
    else
    {
      // sub measure topic
      ProbMsgDataReader_var EmulationReader = ProbMsgDataReader::_narrow(dreader.in());
      ProbMsgSeq msgList;

      // Add datareader statuscondition to waitset
      emulation_sc = EmulationReader->get_statuscondition();
      emulation_sc->set_enabled_statuses(DATA_AVAILABLE_STATUS);
      status = w.attach_condition(emulation_sc);
      status = w.wait(conditionList.inout(), wait_timeout);

      status = EmulationReader->take(msgList, infoSeq, 1,
                                     ANY_SAMPLE_STATE, ANY_VIEW_STATE, ANY_INSTANCE_STATE);
      checkStatus(status, "msgDataReader::take");

      if (msgList.length() > 0 && infoSeq[0].valid_data && msgList[0].destDeviceID == deviceID)
      {
        long probID = msgList[0].probID;
        timespec latency = probingMessage.measure(probID, msgList[0].latency);
        // record pair (msgList[0].srcDeviceID, msgList[0].destDeviceID) latency
        topicRecord.setDeviceID(msgList[0].destDeviceID);
        topicRecord.setLatency(string(msgList[0].topicName), msgList[0].srcDeviceID,
                               latency.tv_sec, latency.tv_nsec);
      }
      status = EmulationReader->return_loan(msgList, infoSeq);
    }
    checkStatus(status, "MsgDataReader::return_loan");
  }

  status = w.detach_condition(emulation_sc);
}

void outputDeviceInfo(Device &deviceRecord, string deviceName)
{
  // create log path
  string path = REPORT_DIR;
  string subTitle = ".log";
  string filename = path + deviceName + "_deviceinfo" + subTitle;
  // create json root
  Json::Value logRoot;
  // set device information
  for (int i = 0; i < deviceRecord.getDeviceSize(); ++i)
  {
    Json::Value record;
    record["cpu"] = deviceRecord.getCPUAt(i);
    record["memory"] = deviceRecord.getMemAt(i);
    record["bandwidth_in"] = deviceRecord.getInBandwidthInfoAt(i) * BYTE;
    record["bandwidth_out"] = deviceRecord.getOutBandwidthInfoAt(i) * BYTE;
    logRoot.append(record);
  }
  // create output file
  fstream file;
  file.open(filename.c_str(), ios::out);
  // write to file
  if (!file)
    cout << "Fail to open file: " << filename << endl;
  else
    file << logRoot.toStyledString() << endl;
  file.close();
}

void getBandwidth(Device &deviceRecord)
{
  while (!gExperimentEnd)
  {
    string rx1, rx2, tx1, tx2;
    string eth = NETWORK_INTERFACE;
    ifstream bandwidth_monitor(string("/sys/class/net/" + eth + "/"));
    if (bandwidth_monitor.good() == 1)
    {
      ifstream rx(string("/sys/class/net/" + eth + "/statistics/rx_bytes"));
      ifstream tx(string("/sys/class/net/" + eth + "/statistics/tx_bytes"));
      if (rx.is_open() && tx.is_open())
      {
        while (!rx.eof())
          rx >> rx1;
        while (!tx.eof())
          tx >> tx1;
      }
      rx.close();
      tx.close();
      sleep(1);
      rx.open(string("/sys/class/net/" + eth + "/statistics/rx_bytes"));
      tx.open(string("/sys/class/net/" + eth + "/statistics/tx_bytes"));
      if (rx.is_open() && tx.is_open())
      {
        while (!rx.eof())
          rx >> rx2;
        while (!tx.eof())
          tx >> tx2;
      }
      rx.close();
      tx.close();
      deviceRecord.setInBandwidth(stoi(rx2) - stoi(rx1));
      deviceRecord.setOutBandwidth(stoi(tx2) - stoi(tx1));
    }
  }
}

void monitorDevice(Device &deviceRecord)
{
  while (!gExperimentEnd)
  {
    deviceRecord.setCPU(getCpuValue());
    deviceRecord.setMem(getMemoryValue());
    sleep(1);
  }
}

int EmulationDataPublisher(int argc, char *argv[])
{
  DDSEntityManager *publisher = nullptr;
  DDSEntityManager *subscriber = nullptr;
  QosSetting *qosSetting = nullptr;
  Topics topicRecord;
  // for Prob.h random
  srand(time(NULL));

  DataSetting *dataSetting = new DataSetting();
  if (loadFile(dataSetting, qosSetting))
  {
    cout << "Device setting file loaded already\n";
    // 1 for measure topic
    publisher = new DDSEntityManager[dataSetting->getPubNum() + 1];
    subscriber = new DDSEntityManager[dataSetting->getSubNum() + 1];

    // init
    Device deviceRecord;
    initCpuValue();
    setQoS(PUBLISH, publisher, dataSetting, qosSetting);
    setQoS(SUBSCRIBE, subscriber, dataSetting, qosSetting);
    Handler *handler = new Handler(publisher, subscriber, dataSetting);

    gExperimentEnd = false;

    // setup bind functions
    vector<Timer> timers_pub;
    vector<Timer> timers_sub;
    function<void()> task;
    queue<ProbContent> probingQueue;
    Probe probingMessage;
    int *measureCount = new int[dataSetting->getPubNum() + 1];

    for (int i = 0; i < dataSetting->getPubNum() + 1; i++)
    {
      string message = "";
      if (i != dataSetting->getPubNum())
        message = dataSetting->getPubMessageAt(i);

      // First time will probe
      measureCount[i] = MEASURE_CONDITION;
      task = bind(publish, ref(publisher[i]), ref(topicRecord),
                  dataSetting->getDeviceID(), message, ref(probingQueue),
                  ref(probingMessage), ref(measureCount[i]));

      int cycleTime;
      // pub measure topic per 1 ms
      if (i != dataSetting->getPubNum())
        cycleTime = dataSetting->getPubFrequencyAt(i);
      else
        cycleTime = 1;

      Timer temp(cycleTime, task);
      timers_pub.push_back(temp);
    }

    for (int i = 0; i < dataSetting->getSubNum() + 1; i++)
    {
      task = bind(subscribe, ref(subscriber[i]), ref(topicRecord),
                  dataSetting->getDeviceID(), ref(probingQueue), ref(probingMessage));

      Timer temp(0, task);
      timers_sub.push_back(temp);
    }

    cout << "preparing\n";
    // start subscriber
    for (auto &timer : timers_sub)
      timer.start();
    // create ready file
    setReady();
    // wait for agentworker create start file
    waitStart();
    cout << "Experiment begin\n";
    std::thread monitor_thread(&monitorDevice, ref(deviceRecord));
    std::thread bandwidth_thread(&getBandwidth, ref(deviceRecord));
    // start publisher
    for (auto &timer : timers_pub)
      timer.start();

    sleep(dataSetting->getExperimentTime());
    gExperimentEnd = true;

    // stop publisher and subscriber
    for (auto &timer : timers_pub)
      timer.end();
    for (auto &timer : timers_sub)
      timer.end();
    monitor_thread.join();
    bandwidth_thread.join();
    cout << "Experiment end\n";

    string deviceName = dataSetting->getDeviceName();
    topicRecord.outputFile(deviceName);
    outputDeviceInfo(deviceRecord, dataSetting->getDeviceName());

    delete handler;
    delete[] measureCount;
    delete[] qosSetting;
    delete[] publisher;
    delete[] subscriber;
  }
  else
    cout << "Load file failed\n";

  delete dataSetting;
  return 0;
}

int OSPL_MAIN(int argc, char *argv[])
{
  return EmulationDataPublisher(argc, argv);
}
