#include "Handler.h"
#define PUBLISH 1
#define SUBSCRIBE 2

using namespace std;

Handler::Handler(DDSEntityManager *publishers, DDSEntityManager *subscribers, DataSetting *dataSetting)
{
	this->publishers = publishers;
	this->subscribers = subscribers;
	this->dataSetting = dataSetting;

	// set pubs
	for (int i = 0; i < this->dataSetting->getPubNum() + 1; i++)
	{
		int topicNameLen;
		string topicName;

		// register prob msg in the last topic
		if (i != this->dataSetting->getPubNum())
		{
			topicNameLen = dataSetting->getPubTopicNameAt(i).length() + 1;
			topicName = dataSetting->getPubTopicNameAt(i).c_str();
			registerMsg(PUBLISH, i);
			setTopic(PUBLISH, topicNameLen, topicName, i);
		}
		else
		{
			registerProbMsg(PUBLISH, i);
			setMeasureTopic(PUBLISH, i);
		}

		setPub(i);
	}

	// set subs
	for (int i = 0; i < this->dataSetting->getSubNum() + 1; i++)
	{
		int topicNameLen;
		string topicName;

		// register prob msg in the last topic
		if (i != this->dataSetting->getSubNum())
		{
			topicNameLen = dataSetting->getSubTopicNameAt(i).length() + 1;
			topicName = dataSetting->getSubTopicNameAt(i).c_str();
			registerMsg(SUBSCRIBE, i);
			setTopic(SUBSCRIBE, topicNameLen, topicName, i);
		}
		else
		{
			registerProbMsg(SUBSCRIBE, i);
			setMeasureTopic(SUBSCRIBE, i);
		}

		setSub(i);
	}
}

Handler::~Handler()
{
	// release pubs
	for (int i = 0; i < this->dataSetting->getPubNum() + 1; i++)
	{
		releasePub(i);
		releaseTopic(PUBLISH, i);
	}

	// release subs
	for (int i = 0; i < this->dataSetting->getSubNum() + 1; i++)
	{
		releaseSub(i);
		releaseTopic(SUBSCRIBE, i);
	}
}

void Handler::registerMsg(int type, int index)
{
	DDSEntityManager *temp = (type == PUBLISH ? this->publishers : this->subscribers);

	MsgTypeSupport_var mt = new MsgTypeSupport();
	temp[index].registerType(mt.in());
}

void Handler::registerProbMsg(int type, int index)
{
	DDSEntityManager *temp = (type == PUBLISH ? this->publishers : this->subscribers);
	ProbMsgTypeSupport_var mt = new ProbMsgTypeSupport();

	temp[index].registerType(mt.in());
}

void Handler::setTopic(int type, int topicNameLen, string topicName, int index)
{
	char *topicNamePtr = new char[topicNameLen];
	strcpy(topicNamePtr, topicName.c_str());
	DDSEntityManager *temp = (type == PUBLISH ? this->publishers : this->subscribers);

	temp[index].createTopic(topicNamePtr);
}

void Handler::setMeasureTopic(int type, int index)
{
	char topicName[] = "measure";
	DDSEntityManager *temp = (type == PUBLISH ? this->publishers : this->subscribers);
	temp[index].createTopic(topicName);
}

void Handler::setPub(int index)
{
	this->publishers[index].createPublisher();
	bool autodispose_unregistered_instances = false;
	this->publishers[index].createWriter(autodispose_unregistered_instances);
}

void Handler::setSub(int index)
{
	this->subscribers[index].createSubscriber();
	this->subscribers[index].createReader();
}

void Handler::releasePub(int index)
{
	this->publishers[index].deleteWriter();
	this->publishers[index].deletePublisher();
}

void Handler::releaseSub(int index)
{
	this->subscribers[index].deleteReader();
	this->subscribers[index].deleteSubscriber();
}

void Handler::releaseTopic(int type, int index)
{
	DDSEntityManager *temp = (type == PUBLISH ? this->publishers : this->subscribers);

	temp[index].deleteTopic();
	temp[index].deleteParticipant();
}
