#ifndef HANDLER_H
#define HANDLER_H

#include <string>
#include <iostream>
#include "DDSEntityManager.h"
#include "ccpp_EmulationData.h"
#include "DataSetting.h"

using namespace EmulationData;

class Handler
{
public:
	Handler(DDSEntityManager *publishers, DDSEntityManager *subscribers, DataSetting *dataSetting);
	~Handler();

	void registerMsg(int type, int index);
	void registerProbMsg(int type, int index);

private:
	void setTopic(int type, int topicNameLen, string topicName, int index);
	void setMeasureTopic(int type, int index);
	void setSub(int index);
	void setPub(int index);
	void releaseSub(int index);
	void releasePub(int index);
	void releaseTopic(int type, int index);
	DDSEntityManager *publishers;
	DDSEntityManager *subscribers;
	DataSetting *dataSetting;
};

#endif