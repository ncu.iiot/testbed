#include "Probe.h"

using namespace std;
mutex mtxTimestamp;

#define NSEC_MAX 1000000000
#define NSEC_HALF 500000000

Probe::Probe()
{
}

Probe::~Probe()
{
	if (this->timestamps.empty())
	{
		this->timestamps.clear();
	}
}

timespec Probe::measure(int probID, long latency)
{
	unique_lock<std::mutex> m_lock(mtxTimestamp);
	timespec timestamp = getTime();
	timespec temp;

	if (timestamps.find(probID) != timestamps.end())
	{
		temp.tv_nsec = timestamp.tv_nsec - this->timestamps[probID].tv_nsec - latency;
		temp.tv_sec = timestamp.tv_sec - this->timestamps[probID].tv_sec;
		// carry
		if (temp.tv_nsec < 0)
		{
			temp.tv_nsec = NSEC_MAX + temp.tv_nsec;
			temp.tv_sec = temp.tv_sec - 1;
		}

		temp.tv_sec = temp.tv_sec / 2;
		if (temp.tv_sec % 2)
		{
			if (NSEC_MAX > temp.tv_nsec / 2 + NSEC_HALF)
			{
				temp.tv_nsec = temp.tv_nsec / 2 + NSEC_HALF;
			}
			else
			{
				temp.tv_nsec = temp.tv_nsec / 2 - NSEC_HALF;
				temp.tv_sec += 1;
			}
		}
	}
	else
	{
		temp.tv_sec = 0;
		temp.tv_nsec = 0;
	}

	m_lock.unlock();
	return temp;
}

timespec Probe::measure(int probID)
{
	unique_lock<std::mutex> m_lock(mtxTimestamp);
	timespec timestamp = getTime();
	timespec temp;

	if (timestamps.find(probID) != timestamps.end())
	{
		temp.tv_nsec = timestamp.tv_nsec - this->timestamps[probID].tv_nsec;
		temp.tv_sec = timestamp.tv_sec - this->timestamps[probID].tv_sec;
		// carry
		if (temp.tv_nsec < 0)
		{
			temp.tv_nsec = NSEC_MAX + temp.tv_nsec;
			temp.tv_sec = temp.tv_sec - 1;
		}

		map<long, struct timespec>::iterator iter = timestamps.find(probID);
		timestamps.erase(iter);
	}
	else
	{
		temp.tv_sec = 0;
		temp.tv_nsec = 0;
	}

	m_lock.unlock();
	return temp;
}

long Probe::addTimestamp()
{
	unique_lock<std::mutex> m_lock(mtxTimestamp);

	timespec timestamp = getTime();
	int hash = rand() % 100000;
	long probID = timestamp.tv_nsec + hash;
	timestamps[probID] = timestamp;

	m_lock.unlock();
	return probID;
}

void Probe::clean()
{
	if (this->timestamps.empty())
	{
		this->timestamps.clear();
	}
}

timespec Probe::getTime()
{
	timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	return now;
}