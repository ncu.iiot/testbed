#ifndef PROBE_H
#define PROBE_H

#include <map>
#include <sys/timerfd.h>
#include <stdlib.h>
#include <mutex>

using namespace std;

class Probe
{
  public:
	Probe();
	~Probe();
	timespec measure(int probID, long latency);
	timespec measure(int probID);
	long addTimestamp();
	void clean();

  private:
	timespec getTime();
	map<long, struct timespec> timestamps;
};

#endif