#include "QosSetting.h"

QosSetting::QosSetting()
{
  reliableKind = 1;
  ownershipKind = 1;
  durabilityKind = 1;
  livelinessKind = 1;
  historyKind = 1;
  depth = 1;
  maxSample = LOCAL_LENGTH_UNLIMITED;
  maxInstance = LOCAL_LENGTH_UNLIMITED;
  maxInstancePerSample = LOCAL_LENGTH_UNLIMITED;
  livelinessSec = LOCAL_DURATION_INFINITE_SEC;
  livelinessNsec = LOCAL_DURATION_INFINITE_NSEC;
  deadlineSec = LOCAL_DURATION_INFINITE_SEC;
  deadlineNsec = LOCAL_DURATION_INFINITE_NSEC;
  latencyBudgetSec = LOCAL_DURATION_INFINITE_SEC;
  latencyBudgetNsec = LOCAL_DURATION_INFINITE_NSEC;
  lifespanSec = LOCAL_DURATION_INFINITE_SEC;
  lifespanNsec = LOCAL_DURATION_INFINITE_NSEC;
  destinationOrderKind = 1;
  value = 0;
}

void QosSetting::setQosSetting(string line)
{
  stringstream ss(line);
  string token, nextToken;

  // parsing order:
  // Reliability, Ownership, Durability, Liveliness, Dead line, History, Resource limit,
  // Latency budget, Lifespan, Destination order, Transport priority

  ss >> token;
  reliableKind = atoi(token.c_str());

  ss >> token;
  ownershipKind = atoi(token.c_str());

  ss >> token;
  durabilityKind = atoi(token.c_str());

  ss >> token;
  livelinessKind = atoi(token.c_str());

  ss >> token;
  ss >> nextToken;
  if (atol(token.c_str()) != 0 || atol(nextToken.c_str()) != 0)
  {
    livelinessSec = atol(token.c_str());
    livelinessNsec = atol(nextToken.c_str());
  }

  ss >> token;
  ss >> nextToken;
  if (atol(token.c_str()) != 0 || atol(nextToken.c_str()) != 0)
  {
    deadlineSec = atol(token.c_str());
    deadlineNsec = atol(nextToken.c_str());
  }

  ss >> token;
  historyKind = atoi(token.c_str());
  ss >> token;
  if (atoi(token.c_str()) > 0)
    depth = atoi(token.c_str());

  ss >> token;
  if (atol(token.c_str()) != 0)
    maxSample = atol(token.c_str());
  ss >> token;
  if (atol(token.c_str()) != 0)
    maxInstance = atol(token.c_str());
  ss >> token;
  if (atol(token.c_str()) != 0)
    maxInstancePerSample = atol(token.c_str());

  ss >> token;
  ss >> nextToken;
  if (atol(token.c_str()) != 0 || atol(nextToken.c_str()) != 0)
  {
    latencyBudgetSec = atol(token.c_str());
    latencyBudgetNsec = atol(nextToken.c_str());
  }

  ss >> token;
  ss >> nextToken;
  if (atol(token.c_str()) != 0 || atol(nextToken.c_str()) != 0)
  {
    lifespanSec = atol(token.c_str());
    lifespanNsec = atol(nextToken.c_str());
  }

  ss >> token;
  destinationOrderKind = atoi(token.c_str());

  ss >> token;
  value = atoi(token.c_str());

  // durabilityService
  ss >> token;
  ss >> nextToken;
  if (atol(token.c_str()) != 0 || atol(nextToken.c_str()) != 0)
  {
    durabilityServiceSec = atol(token.c_str());
    durabilityServiceNsec = atol(nextToken.c_str());
  }

  ss >> token;
  durabilityServiceHistoryKind = atoi(token.c_str());

  ss >> token;
  if (atoi(token.c_str()) > 0)
    durabilityServiceHistoryDepth = atoi(token.c_str());

  ss >> token;
  if (atoi(token.c_str()) > 0)
    durabilityServiceMaxSamples = atoi(token.c_str());

  ss >> token;
  if (atoi(token.c_str()) > 0)
    durabilityServiceMaxInstances = atoi(token.c_str());

  ss >> token;
  if (atoi(token.c_str()) > 0)
    durabilityServiceMaxSamplesPerInstance = atoi(token.c_str());

  // topicData
  ss >> token;
  topicDataValue = token;
}

bool isDurationValid(long sec, unsigned long nsec)
{
  return (sec != 0 || nsec != 0);
}

int QosSetting::getReliableKind()
{
  return reliableKind;
}

int QosSetting::getOwnershipKind()
{
  return ownershipKind;
}

int QosSetting::getDurabilityKind()
{
  return durabilityKind;
}

long QosSetting::getDurabilityServiceSec()
{
  return durabilityServiceSec;
}

unsigned long QosSetting::getDurabilityServiceNsec()
{
  return durabilityServiceNsec;
}

int QosSetting::getDurabilityServiceHistoryKind()
{
  return durabilityServiceHistoryKind;
}

long QosSetting::getDurabilityServiceHistoryDepth()
{
  return durabilityServiceHistoryDepth;
}

long QosSetting::getDurabilityServiceMaxSamples()
{
  return durabilityServiceMaxSamples;
}

long QosSetting::getDurabilityServiceMaxInstances()
{
  return durabilityServiceMaxInstances;
}

long QosSetting::getDurabilityServiceMaxSamplesPerInstance()
{
  return durabilityServiceMaxSamplesPerInstance;
}

int QosSetting::getLivelinessKind()
{
  return livelinessKind;
}

int QosSetting::getHistoryKind()
{
  return historyKind;
}

int QosSetting::getDestinationOrderKind()
{
  return destinationOrderKind;
}

long QosSetting::getLivelinessSec()
{
  return livelinessSec;
}

unsigned long QosSetting::getLivelinessNsec()
{
  return livelinessNsec;
}

long QosSetting::getDeadlineSec()
{
  return deadlineSec;
}

unsigned long QosSetting::getDeadlineNsec()
{
  return deadlineNsec;
}

long QosSetting::getHistoryDepth()
{
  return depth;
}

long QosSetting::getResourceLimitSample()
{
  return maxSample;
}

long QosSetting::getResourceLimitInstance()
{
  return maxInstance;
}

long QosSetting::getResourceLimitSamplePerInstance()
{
  return maxInstancePerSample;
}

long QosSetting::getLatencyBudgetSec()
{
  return latencyBudgetSec;
}

unsigned long QosSetting::getLatencyBudgetNsec()
{
  return latencyBudgetNsec;
}

long QosSetting::getLifespanSec()
{
  return lifespanSec;
}

unsigned long QosSetting::getLifespanNsec()
{
  return lifespanNsec;
}

long QosSetting::getTransportPriorityValue()
{
  return value;
}

string QosSetting::getTopicDataValue()
{
  return topicDataValue;
}

// test
void QosSetting::print_QosSetting()
{
  cout << "reliableKind:" << reliableKind << endl;
  cout << "ownershipKind:" << ownershipKind << endl;
  cout << "durability_kind:" << reliableKind << endl;
  cout << "livelinessKind:" << livelinessKind;
  cout << " livelinessSec:" << livelinessSec;
  cout << " livelinessNsec:" << livelinessNsec << endl;
  cout << "deadlineSec:" << deadlineSec;
  cout << " deadlineNsec:" << deadlineNsec << endl;
  cout << "historyKind:" << historyKind;
  cout << " depth:" << depth << endl;
  cout << "maxSample:" << maxSample;
  cout << " maxInstance:" << maxInstance;
  cout << " maxInstancePerSample:" << maxInstancePerSample << endl;
  cout << "latencyBudgetSec:" << latencyBudgetSec;
  cout << " latencyBudgetNsec:" << latencyBudgetSec << endl;
  cout << "destinationOrderKind:" << destinationOrderKind << endl;
  cout << "lifespanSec:" << lifespanSec;
  cout << " lifespanNsec: " << lifespanNsec << endl;
  cout << "TransportPriorityValue:" << value << endl;
}