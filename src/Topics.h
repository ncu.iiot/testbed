#ifndef TOPICS_H
#define TOPICS_H

#include <vector>
#include <set>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <iostream>
#include <map>
#include <jsoncpp/json/json.h>
#include "sys/timerfd.h"

using namespace std;

struct Record
{
  int pubCount;
  map<int, int> subCount;
  map<int, set<int>> subMsgID;
  map<int, vector<timespec>> latencies;
};

class Topics
{
public:
  Topics();
  ~Topics();
  void setDeviceID(int deviceID);
  void setLatency(string topicName, int deviceID, time_t sec, long nsec);
  void addPubCount(string topicName);
  void addSubCount(string topicName, int deviceID);
  bool addSubMsgID(string topicName, int deviceID, int msgID);
  void outputFile(string deviceName);

  int getDeviceID();
  int getLatencySize(string topicName);
  int getPubCount(string topicName);
  map<int, int> getSubCount(string topicName);
  map<int, set<int>> getSubMsgID(string topicName);
  vector<int> getLatencyIndices(string topicName);
  vector<timespec> getLatencyAt(string topicName, int latencyIndex);

private:
  void insertRecord(string topicName);
  void insertSubConut(string topicName, int deviceID);
  int deviceID;
  map<string, Record> records;
};

#endif